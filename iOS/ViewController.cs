﻿using System;

using UIKit;
using Foundation;

namespace OnMyWay.iOS
{
    public class WebViewDelegate : UIWebViewDelegate
    {
        public override void LoadFailed (UIWebView webView, NSError error) {
            var msg = "xxx";
        }
        public override void LoadingFinished (UIWebView webView) { }
        public override void LoadStarted (UIWebView webView) { }
        public override bool ShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType) {
            return true;
        }
    }

    public partial class ViewController : UIViewController
    {
        private WebViewDelegate wvDelegate;
        public ViewController (IntPtr handle) : base (handle)
        {
            wvDelegate = new WebViewDelegate ();
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();

            // Code to start the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start ();
#endif

            // Perform any additional setup after loading the view, typically from a nib.
            webView.AccessibilityIdentifier = "map";
            webView.Delegate = this.wvDelegate;
            var urlString = omwlib.AppController.initialUrl ();
            var url = new Foundation.NSUrl(urlString);
            var request = Foundation.NSUrlRequest.FromUrl (url);
            webView.LoadRequest (request);
        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.		
        }
    }
}
