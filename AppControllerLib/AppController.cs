﻿using System;
namespace AppControllerLib
{
    public interface PlatformDelegate
    {
        void refresh(string httpMethod, string url, string content);
    }

    public class AppController
    {
        private PlatformDelegate platformDelegate;
        public string name { get; set; }
        public AppController (PlatformDelegate pd)
        {
            this.platformDelegate = pd;
        }

        static public string initialUrl ()
        {
            return "http://onmyway-timlittlefair.rhcloud.com";
        }
    }
}

